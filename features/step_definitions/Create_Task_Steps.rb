Given(/^I as Todo app user$/) do
  visit("http://qa-test.avenuecode.com")
  click_on("Sign In")
  fill_in('user_email',:with => 'lufer.jom@gmail.com')
  fill_in('user_password',:with => 'q1w2e3r4')
  click_on("Sign in")
  assert_text('Signed in successfully.')
  page.has_xpath?('//ul[contains(@class, "navbar-right")]/li/a')
  assert_text('Welcome, Luiz Fernando!')
  page.has_content?('
            ToDo App
          ')
  page.has_content?('
            Demo ToDo app using Ruby on Rails and Angular JS.
          ')
  page.has_xpath?('//a[contains(@class, "btn-success")]')
end

When(/^I create a task$/) do
  find(:xpath, '//a[contains(@class, "btn-success")]').click
  fill_in('new_task', :with =>'Luiz Task')
  find(:xpath, '//span[contains(@ng-click, "addTask")]').click
end

Then(/^it have to display in my Todo List$/) do
  page.has_content?('Luiz Task')
end