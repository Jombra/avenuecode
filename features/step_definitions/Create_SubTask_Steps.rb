When(/^I access my Manage Sub\-task$/) do
  find(:xpath, '//a[contains(@class, "btn-success")]').click
  find(:xpath, '//*[text() = "Luiz Task"]/ancestor::tr/td/button[contains(text(), "Manage Subtasks")]').click
  should have_content('Editing Task ')
  has_content?('Todo:')
  find(:id,'edit_task')
  should have_content('SubTask Description:')
  should have_content('Due Date:')
end

When(/^I create a Sub\-Task$/) do
  fill_in('new_sub_task', :with =>'Sub Task Luiz')
  fill_in('dueDate', :with => '03/08/2017')
  find(:id, 'add-subtask').click
  should have_content('Done')
  should have_content('SubTask')
  should have_content('Remove SubTask')
  has_xpath?('//a[contains(@class, editable-click) and text() = "Sub Task Luiz"]')
end

Then(/^I able to my sub\-task in my dashboard$/) do
  find(:xpath, '//button[contains(@class, "btn-primary ng-binding") and text() = "(1) Manage Subtasks"]')
end