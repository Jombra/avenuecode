#language: en
#utf-8
@SubTask
Feature: Create Sub-task
	As a ToDo App user
	I should be able to create a subtask
	So I can break down my tasks in smaller piecess


Background:
		Given I as Todo app user

Scenario: Create Sub-task
	When I access my Manage Sub-task
	And I create a Sub-Task
	Then I able to my sub-task in my dashboard